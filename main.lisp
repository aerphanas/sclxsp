(defpackage :sclxsp
  (:use :cl))

(in-package :sclxsp)

(require 'clx)

(defvar *display*
  (xlib:open-default-display))

(defvar *window*
  (let ((rwind (xlib:screen-root (car (xlib:display-roots *display*)))))
    (xlib:create-window
     :parent rwind
     :x 10
     :y 10
     :width 200
     :height 200
     :border-width 1)))

(defun main ()
  (xlib:change-property *window* :clxget '() :utf8_string 32)
  (xlib:convert-selection :primary :utf8_string *window* :clxget)
  (xlib:get-property *window* :clxget)
  (let ((clipboard (coerce (mapcar #'code-char (xlib:get-property *window* :clxget))
                           'string)))
    (xlib:delete-property *window* :clxget)
    (format t "~A~%" clipboard)))

(main)
(xlib:destroy-window *window*)
(xlib:close-display *display*)
